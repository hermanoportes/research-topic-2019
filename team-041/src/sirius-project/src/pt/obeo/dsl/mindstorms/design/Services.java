package pt.obeo.dsl.mindstorms.design;

import java.util.List;

import org.eclipse.emf.ecore.EObject;

import pt.isep.edom.mindstorms.Choreography;
import pt.isep.edom.mindstorms.GoForward;
import pt.isep.edom.mindstorms.Instruction;
import pt.isep.edom.mindstorms.Rotate;

/**
 * The services class used by VSM.
 */
public class Services {
    
	public Instruction getNextInstruction(Instruction instruction) {
		Choreography parentChoreography=(Choreography)instruction.eContainer();
		List<Instruction> actions=parentChoreography.getInstructions();
		int position=actions.indexOf(instruction);
		if (position==actions.size()-1) {
			return null;
		}
		else {
			return actions.get(position+1);
		}
	}
	
	public String getLabel(Instruction instruction) {		
		if (instruction instanceof GoForward) {
			return ((GoForward)instruction).getCm()+" cm";
		}
		else if (instruction instanceof Rotate) {
			if (((Rotate)instruction).isRandom())
				return "?";
			else
				return ((Rotate)instruction).getDegrees()+"�";
		}
		else if (instruction instanceof Choreography) {
			return ((Choreography)instruction).getName();
		}
		else
			return "";
	}
}
