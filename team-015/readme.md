
# EDOM - Project Research Topic

**Topic**: M2T-JET

## Índice
    1. Introdução ao Model-to-Text (M2T) Transformation Project
    2. O Java Emitter Templates - JET
    3. Instalando o JET
    4. Utilização do JET
    5. Alternativas ao JET
    6. Bibliografia.

---

## 1. Introdução ao Model-to-Text (M2T) Transformation Project
O Model-to-Text Tranformation Project, ou M2T, é um projeto open source, parte do Eclipse Modeling Project, focado na geração de artefactos textuais através da tranformação de modelos, disponibilizando ferramentas e infraestrutura necessárias para tal.

O desenvolvimento orientado a modelos, ou Model Driven Development (MDD) e de Model Drive Architecture (MDA), definido pela Object Management Group (OMG), estimulam a utilização de modelos, seus respectivos refinamentos e transformações para auxiliar no desenvolvimento de aplicações. Enquanto o Model-to-Model (M2M) é focado em tecnologias de transformação entre modelos, o M2T foca nas tranformações destes modelos em texto, geralmente código fonte (e.g. Java, C++) e recursos consumidos pela aplicação. 

Soluções para transformações model-to-text geralmente utilizam "templates" para extrair informação de modelos. Uma das preocupações do M2T é a redução do esforço envolvido no desenvolvimento e manutenção de templates.

### 1.1. Tecnologias envolvidas nas soluções Model-to-Text

Os templates são apenas uma das parte envolvidas nas soluções de transtormações Model-to-Text.

Essas soluções podem ainda incluir mecanismos para acesso a diferentes modelos, navegação, alteração de modelos existentes, entre outos.

Várias ferramentas para integração de código gerado com código escrito manualmente também são disponibilizadas, como gestão de regiões protegidas e o suporte á mesclagem destes.

### 1.2. Sub-projetos do M2T

O Model-to-Text nunca teve a pretensão de se tormar um depósito para 'template languages', por isso incluiu apenas algumas, consideradas estratégicas, fazendo uso das seguintes implementações de templates, utilizadas nas tranformações M2T :
1. Java Emitter Templates (JET), derivado do Java Server Pages (JSP), inicialmente desenvolvido como parte do Eclipse Modelling Framework (EMF);
2. Acceleo, um gerador de código que implementa o padrão 'OMG's MOF Model to Text Language' (MTL).
3. Xpand,  originalmente desenvolvido como parte do projeto openArchitectureWare, antes de se tornar um componente do eclipse.

Para cada 'template language', o M2T também disponibiliza:

- Integração com o M2T Invocation Framework;
- Editores de templates.
	
## 2. O Java Emitter Templates - JET 

### 2.1. Introdução

O Java Emitter Templates (JET) é uma ferramenta de template, parte do Eclipse Modeling Framework, usada na implementação de um "gerador de código", um importante componente do desenvolvimento orientado a modelo (Model Driven Development - MDD), que auxilia na automatização de processos de transformação model-to-text, acelerando tal processo.

Entretanto, essas transformações não são sempre perfeitas, podendo variar a depender do contexto em que é aplicada, podendo, esse problema, ser resolvido com a inclusão algum mecanismos para auxílio às modificações no gerador de código, com a utilização de templates. A função do JET é permitir o usuário substituir, segundo suas necessidades, as implementações de templates, disponibilizando:

- Expansão da linguagem JET  para suportar tags customizadas;
- Definição de interfaces Java e  pontos de extensão do Eclipse para declarar biblioteca de tags customizadas;
- Bibliotecas de tag padrão do JET, que possibilitam a criação de transformações sem recursos do Java ou APIs do Eclipse.
- API para o Eclipse e UI para invocar tais transformações.
- Editor do JET template (inicialmente um componente separado, EMFT JET Editor).

 Main packages

![Main packages](https://bitbucket.org/mei-isep/edom-19-20-atb-15/raw/a71f5e7bf29f/project-research-topic/img/packages.png)

Other packages

![Other packages](https://bitbucket.org/mei-isep/edom-19-20-atb-15/raw/a71f5e7bf29f/project-research-topic/img/otherpackages.png)

### 2.2. Proposta de melhoramento do JET (JET2)

![Revision History](https://bitbucket.org/mei-isep/edom-19-20-atb-15/raw/a71f5e7bf29f/project-research-topic/img/revisionhistory.png)

Algumas limitações do JET:

- Opera no contexto de programas Java, o que representa uma barreira para usuários em outros contextos;
- Requer conhecimento em outras ferramentas como Eclipse Resource e APIs;
- Apesar de inspirado no Java Server Pages (JSP), falta-lhe bibliotecas de tags e tags customizadas.

A arquitetura proposta  para o JET2 consiste em 5 partes:

1. Compilar a linguagem JET2 para Java
2. Interfaces de Biblioteca de Tags, classes e pontos de extensão
3. JET2 Parser
4. Estrutura do projeto JET2 e Builders
5. Carregar e executar templates JET2 em um ambiente de desenvolvimento.

#### 2.2.1. Compilando a Linguagem JET2 para Java

Cada template JET2 é compilado para uma classe Java. Apesar de similar ao JSP, as práticas de compilação não são sempre as mesmas, já que além de operar em um ambiente diferente daquele do JET2, o JSP corre em um contexto de web servlet, o que não ocorre no JET2. 

JET2 abstract Syntax Tree
![JET2 Abstract Syntax Tree (AST)](https://bitbucket.org/mei-isep/edom-19-20-atb-15/raw/a71f5e7bf29f/project-research-topic/img/abstractsyntaxtree.png)

#### Regras para emitir Templates

- Emite uma classe Java por template;
- A classe java deve ter um 'generate' público, que aceita argumentos e produz o resultado da expansão do template;
- O método inclui código de inicialização. O corpo do template é emitido por esse método;
- O conteúdo do template é escrito nesse método.

JET 'generate' method signatureabstractsyntaxtree

![JET 'generate' method signature](https://bitbucket.org/mei-isep/edom-19-20-atb-15/raw/a71f5e7bf29f/project-research-topic/img/currentgeneratemethod.png)

JET2 'generate' method signature

![JET2 'generate' method signature](https://bitbucket.org/mei-isep/edom-19-20-atb-15/raw/a71f5e7bf29f/project-research-topic/img/generatemethod.png)

#### Regras para emitir Texto

- Texto do template pode ser escrito diretamente no proprio 'writer' (e.g. out.write("Some text")).

#### Regras para emitir Expressões Java e Scripts

- Expressões Java e scripts devem ser emitidos no método 'generate()'

#### Standard prolog  para emitir Tags

- Emitir variável do tipo tag e atribuir nova instância da classe tag;
- Chamar o método setParent, passando uma variável tag ou null
- Chamar o método setContext, passando o contexto do template atual;
- Inicializar atributos da tag, chamando métoddos (e.g. setXXX());
- Chamar o método doStartTag() e salvar o resultado em uma variável local. Esse método pode retornar os valores  EVAL_BODY, SKIP_BODY ou EVAL_BODY_BUFFERED.

Standard prolog for emitting Tags

![JET2 Standard prolog for emitting Tags](https://bitbucket.org/mei-isep/edom-19-20-atb-15/raw/a71f5e7bf29f/project-research-topic/img/emittingtag.png)

Emitting Tag without a body

![Emitting Tag Without body](https://bitbucket.org/mei-isep/edom-19-20-atb-15/raw/a71f5e7bf29f/project-research-topic/img/emittingtag2.png)

Emitting Tag with a body

![Emitting Tag Without body](https://bitbucket.org/mei-isep/edom-19-20-atb-15/raw/a71f5e7bf29f/project-research-topic/img/emittingtag3.png)

Tags that process the body content

![Tags that process the body content](https://bitbucket.org/mei-isep/edom-19-20-atb-15/raw/a71f5e7bf29f/project-research-topic/img/emittingtag4.png)

Iterating tags

![Iterating tags](https://bitbucket.org/mei-isep/edom-19-20-atb-15/raw/a71f5e7bf29f/project-research-topic/img/emittingtag5.png)

#### 2.2.2. Interfaces de Biblioteca de Tags, Classes e Pontos de Extensão

![ Interfaces e Classes](https://bitbucket.org/mei-isep/edom-19-20-atb-15/raw/a71f5e7bf29f/project-research-topic/img/taginterfaces.png)

#### 2.2.3. JET2 Parser

#### 2.2.4. Estrutura do Projeto JET2 e Builders

JET2 utiliza a infraestrutura do Eclipse para compilar classes java geradas em código, enquanto o carregamento do JET2 dynamic template  se dá utilizando 'OSGi dynamic loading/unloading of Bundles'. Isso implica que o 'builder' de um projeto JET2 precisa produzir um 'OSGI bundle' válido.

Configuração de dependências de plug-ins extra podem ser feitas usando mecanismos padrão, como o 'bundle manifest'.

#### 2.2.5. Carregando e Executando Templates no Ambiente de Desenvolvimento

O Carregamento é realizado através do OSGi framework. Para isso, é necessário um BundleContext. Ao correr o plugin, org.eclipse.core.runtime.Plugin.startup(BundleContext) é chamado. O plugin JET2 responsável pelo carregamento dinâmico guarda esse 'bundle context' para futuro carregamento de um JET2 bundle.
uma vez que o BundleContext está á mão, um bundle pode ser carregado chamando org.osgi.framework.installBundle(String), que retorna um org.osgi.framework.Bundle. Em caso de sucesso, o bundle é iniciado chamando  org.osgi.framework.Bundle.start().
Um bundle pode ainda ser desinstalado (org.osgi.framework.Bundle.uninstall()), o que implica na interrupção do mesmo (org.osgi.framework.Bundle.stop()).

## 3. Instalando o JET
Para instalação do JET na IDE Eclipse deve-se acessar o menu Help > Install New Software... e adicionar um novo sitio, nomeando o repositório e indicando no path o endereço [http://download.eclipse.org/modeling/m2t/updates/releases/](http://download.eclipse.org/modeling/m2t/updates/releases/). Após carregado(s) o(s) pacote(s) disponível(s), deve-se selecionar aqueles a serem instalados e clicar em Next.

![Install Package](https://bitbucket.org/mei-isep/edom-19-20-atb-15/raw/a71f5e7bf29f/project-research-topic/img/installpackage.png)

Após verificação das dependências, o instalador mostrará os detalhes das alterações a serem efetuadas.

![Install Details](https://bitbucket.org/mei-isep/edom-19-20-atb-15/raw/a71f5e7bf29f/project-research-topic/img/installdetails.png)

Ao finalizar a instalação, reinicie a instância do Eclipse.

## 4. Utilização do JET

Tentaremos apresentar aqui, através de FAQs (Frequent Asked Questions), alguns casos de uso do template.

### 4.1. Como criar uma tag?

1. Crie um projeto tipo plug-in

2. No editor do manifest, adicione, no separador dependencies: org.eclipse.jet, org.eclipse.core.runtime, org.eclipse.core.resources. 

3. Ainda no manifest, vá ao separador extensions e adicione: org.eclipse.jet.taglibraries.

4. Crie uma tagLibrary, atribua uma identificação, como "MyCustomWorkspaceTags", e um prefixo tal como mcws.

5. Na tagLibrary, crie uma tag com o nome e tipo desejados.

5. Adicione, na 'Tag node', quantos atributos forem necessários e configure algumas opções.

6. Retorne a conditionalTag, e click na 'class entry' para criar a classe Java para a tag. Especifique o nome da classe e confira o package.

7. Escreva a classe Java.

![How to create a tag in JET](https://bitbucket.org/mei-isep/edom-19-20-atb-15/raw/a71f5e7bf29f/project-research-topic/img/createtag.png)

### 4.2. Como correr uma transformação através do Java?

Projetos JET são plugins do Eclipse, o que o torna dependente do Eclipse, não sendo possível correr um projeto deste tipo fora do Eclipse.

A classe JET2Patform possui vários métodos runTransformOnXXX que disparam uma transformação de maneira programada.

#### runTransformOnResource

Usa um arquivo do workspace como entrada para uma transformação.

#### runTransformOnObject
É usado quando já se tem um modelo carregado na memória.

#### runTransformOnString

Esse método pega um modelo (e.g. um documento XML) como uma string,  e corre a transformação. Assim como no  runTransformOnObject,se estás a usar variáveis do org.eclipse.jet.resource na sua transformação, tens que configura-lo manualmente.

## 5. Alternativas ao JET

Existe uma grande variedade de linguagens de template disponíveis para a realização de transformações model-to-text, algumas delas já citadas, como os outros sub-projetos do Eclipse M2T Project, Acceleo e Xpand.

#### Acceleo

Acceleo é um gerador de código que implementa a especificação 'OMG's Model-to-text', criado e desenvolvido pela Obeo, em 2005. Ele fornece ao desenvolvedor funcionalidades da mais alta qualidade que se espera em um IDE para geração de código: sintaxe simples, geração de código eficiente, ferramentas avançadas, etc

#### Xpand

Xpand é uma linguagem de transformação especializada em geração de código, baseada em modelos EMF. O Xpand suporta diversas características, como 'Pluggable Type System', 'Dynamic Dispatch of Functions', 'Rich Expressions' (parecidas com a sintaxe do OCL mas com sintaxe Java), foi originalmente desenvolvido como parte part do projeto openArchitectureWare antes de se tornar um componente do Eclipse. 

#### MOFM2T

É uma ferramenta para transformação model to text, baseada no EMF e no Ecore, que suporta a geração de código de implementação e de documentação a partir de modelos, além de transformações model to model. A ferramenta fornece uma linguagem independente do metamodelo, que permite usar qualquer tipo de metamodelo ou suas instâncias para gerar transformações de modelo para texto ou outros modelos.

## 6. Conclusão

O projeto JET foi arquivado recentemente, após anos sem novos releases, restando ao usuário a utilização de outras alternativas para a realização de transformações Model-to-Text, como o próprio Acceleo ou o Xpand, também sub-projetos do Eclipse M2T Project.

![How to create a tag in JET](https://bitbucket.org/mei-isep/edom-19-20-atb-15/raw/a71f5e7bf29f/project-research-topic/img/terminationreview.png)

## 7. Bibliografia

- [https://www.eclipse.org/modeling/m2t/](https://www.eclipse.org/modeling/m2t/)
- [https://www.eclipse.org/proposals/m2t/](https://www.eclipse.org/proposals/m2t/)
- [https://projects.eclipse.org/projects/modeling.m2t.jet](https://projects.eclipse.org/projects/modeling.m2t.jet)
- [https://www.eclipse.org/modeling/emf/docs/architecture/jet2/jet2.html](https://www.eclipse.org/modeling/emf/docs/architecture/jet2/jet2.html)
- [https://download.eclipse.org/modeling/m2t/jet/javadoc/1.2.0/](https://download.eclipse.org/modeling/m2t/jet/javadoc/1.2.0/)
- [https://www.eclipse.org/modeling/m2t/downloads/](https://www.eclipse.org/modeling/m2t/downloads/)
- [https://www.ibm.com/support/knowledgecenter/SSZLC2_9.0.0/com.ibm.commerce.component-services.doc/tasks/twvjet.htm](https://www.ibm.com/support/knowledgecenter/SSZLC2_9.0.0/com.ibm.commerce.component-services.doc/tasks/twvjet.htm)
- [https://edisciplinas.usp.br/pluginfile.php/4138138/mod_resource/content/1/Aula12-MDSE-book-slides-chapter9.pdf](https://edisciplinas.usp.br/pluginfile.php/4138138/mod_resource/content/1/Aula12-MDSE-book-slides-chapter9.pdf)
- [https://wiki.eclipse.org/JET_FAQ_How_do_I_create_custom_tag%3F](https://wiki.eclipse.org/JET_FAQ_How_do_I_create_custom_tag%3F)
- [https://wiki.eclipse.org/JET_FAQ_How_do_I_run_a_JET_transformation_from_Java%3F](https://wiki.eclipse.org/JET_FAQ_How_do_I_run_a_JET_transformation_from_Java%3F)
- [https://marketplace.eclipse.org/content/mofscript-model-transformation-tool](https://marketplace.eclipse.org/content/mofscript-model-transformation-tool)
- [https://projects.eclipse.org/projects/modeling.m2t.acceleo](https://projects.eclipse.org/projects/modeling.m2t.acceleo)
- [https://projects.eclipse.org/projects/modeling.m2t.xpand](https://projects.eclipse.org/projects/modeling.m2t.xpand)


