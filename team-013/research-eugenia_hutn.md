# DSL #

## DSL Gráfica - EuGENia ##

O EuGENia é uma ferramenta que gera automaticamente os modelos .gmfgraph, .gmftool e .gmfmodel necessários para implementar um editor GMF a partir de um metamodelo Ecore. Nas imagens seguintes é possível ver um exemplo de um metamodelo de Sistemas de Arquivos e o seu respetivo editor visual gerado:

**Metamodelo** (código disponível neste [link](https://git.eclipse.org/c/epsilon/org.eclipse.epsilon.git/tree/examples/org.eclipse.epsilon.eugenia.examples.filesystem/model/filesystem.ecore))  
![eugenia-gmfEditor_example](img/eugenia-metamodel.png)

**Editor GMF**  
![eugenia-gmfEditor_example](img/eugenia-gmfEditor_example.png)

Esta ferramenta não suporta todas as funcionalidades do GMF visto que, caso as suportasse, apenas tornaria o EuGENia tão complexo quanto o GMF (que é exatamente o que não se pretende ao usá-lo). No entanto, é possível costumizar o editor de uma forma sistemática. Criando uma tranformação EOL ("ECore2GMF.eol") e colocando-a na mesma pasta que o diagrama ecore, o EuGENia usa-a para costumizar a geração do editor GMF. É também possível atribuir imagens a nós em vez de formas que é a opção default.

### Anotações ###

É através das anotações que o EuGENia cria os elementos do GMF, possibilitando que os iniciantes criem mais facilmente um editor. Um exemplo de como aplicar as anotações pode ser encontrado no [link](https://git.eclipse.org/c/epsilon/org.eclipse.epsilon.git/tree/examples/org.eclipse.epsilon.eugenia.examples.filesystem/model/filesystem.ecore) do código do metamodelo acima ilustrado.

Anotações suportadas:

* **gmf**: aplicado ao topo do EPackage, indica que são esperadas anotações relacionadas com o GMF;
* **gmf.diagram**: indica o objeto root do metamodelo (apenas pode haver 1);
* **gmf.node**: indica que deve aparecer no diagrama como um *node*;
* **gmf.link**: aplicado em EClasses que devem aparecer como links;
* **gmf.compartment** (for containment EReference): indica que a referência de *containment* criará um compartimento onde os elementos do modelo serão colocados;
* **gmf.affixed** (for containment EReference): indica que a referência de *containment* irá criar *nodes* que são afixados às pontas do *node* no qual se encontra contido;
* **gmf.label** (for EAttribute): define rótulos adicionais para a EClass na qual se contra contido.

Fontes:

* [Documentação do EuGENia](https://www.eclipse.org/epsilon/doc/articles/#eugenia)
* [Apresentação do Dimitris Kolovos (Epsilon lead developer)](https://www.slideshare.net/dskolovos/eugenia)

## DSL Textual - HUTN ##

O Human Usable Textual Notation (HUTN) é um *standard* para armazenar modelos de uma forma conpreensível pelo ser-humano. Por outras palavras, é uma alternativa ao XMI mas orientado para humanos. O Epsilon fornece uma implementação do HUTN realizada usando o ETL, EGL e EVL.

Depois da criação do metamodelo, cria-se um novo documento para esse mesmo metamodelo clicando **File -> New -> Other... -> HUTN File**.

### Example ###

**Metamodelo**  
![hutn-metamodel](img/hutn-metamodel.png)

**Conteúdo do ficheiro gerado**:

```hutn
@Spec {
  metamodel "families" {
    nsUri: "families"
  }
}

families {
  // Place your model element specifications here
}
```

On the families section it is now possible to add various `Family` elements such as:

```hutn
Family {
  name: "The Simpsons"
  lotteryNumbers: 10, 24, 26, 32, 45, 49
  members: Person{
             name: "Homer"
            },Person{
             name: "Marge"
            },Person{
             name: "Bart"
            },Person{
             name: "Liza"
            },Person{
             name: "Maggie"
            }
}
```

It is also possible to use cross-model references using a URI fragment:

```hutn
Family {
  familyFriends: Family "../families/AnotherNeighbourhood.model#/1/"
}
```

Para gerar o modelo a partir de um ficheiro .hutn, clica-se com o botão direito do rato e seleciona-se **HUTN -> Generate Model**.

### Configuração de Documentos HUTN ###

É também possível configurar documentos HUTN. Tomando como o exemplo o seguinte fiheiro .hutn que usa o metamodelo das famílias acima ilustrado, podemos verificar que existe bastante informação duplicada:

```hutn
@Spec {
  metamodel "families" {
    nsUri: "families"
  }
}

families {
  Family {
    name: "The Smiths"
    familyFriends: Family "does", Family "bloggs"
  }
  
  Family "does" {
    name: "The Does"
    migrant: true
  }
  
  Family "bloggs" {
    name: "The Bloggs"
    migrant: true
  }
}
```

Através da criação de um modelo de configuração é possível reduzir a quantidade de informação duplicada. Esta configuração usa regras que contumizam o documento .hutn. Para tal, deve-se criar um modelo EMF com a seguinte configuração (aplicada a cada caso):

![Criação do modelo emf](img/hutn-emfModel.png)

Na nova configração podem-se adicionar novas regras, tais como atribuir um valor default a um atributo ou regras de identificação. Um exemplo poderá ser adicionar uma regra para atribuir o valor `true` como default ao atributo `migrant` e adicionar uma regra de identificação à família na qual se utiliza o atributo `name` para a identificação de uma família. O código seguinte representa a atualização do exemplo anteriormente apresentado:

```hutn
@Spec {
  metamodel "families" {
    nsUri: "families"
    configFile: "FamiliesConfig.model"
  }

  families {
    Family {
      name: "The Smiths"
      familyFriends: Family "The Does", Family "The Bloggs"
      migrant: false
    }

    Family "The Does" {}

    Family "The Bloggs" {}
  }
}
```

Fontes:

* [Documentação HUTN pelo Epsilon](https://www.eclipse.org/epsilon/doc/articles/#hutn)

**Voltar para o readme.md principal**

* [https://bitbucket.org/mei-isep/research-topic-2019/src/master/team-013/](README.md)
