# EVL #

A validação de um modelo contra o seu metamodelo é um dos passos mais importantes para descobrir inconsistências no modelo. 
Muitas vezes o enriquecimento de um metamodelo usando invariantes OCL pode detectar algumas inconsistências, no entanto isso tem várias limitações, o que motiva a criação de uma linguagem para validação de modelos usando o Epsilon, à qual se chamou Epsilon Validation Language (EVL).

### Motivação

OLC é um dos standards da industria para a validação de modelos, permitindo capturar constraints da linguagem de modelação de forma clara e concisa.
As constraints estruturais são capturadas sob a forma de invariantes, onde cada invariante é definida no contexto de uma meta-classe do metamodelos, e especifica um nome e um body.
O corpo deve devolver um valor boleano, que indica se a invariante é satisfeita. 
Apesar disso, a sua natureza declarativa e livre de side-effects, introduz várias limitações, tais como:

- Feedback limitado: não permite devolver mensagens significativas, no caso da invariante não ser satisfeita. 
- Não suporta warnings: em situações em que não há um problema crítico, mas mesmo assim um problema que deve ser analisado pelo utilizador, não permite uma discriminação entre erros e warnings, sendo que todos os problemas serão marcados como erro. Isto adiciona um trabalho extra em identificar todos os problemas e prioritizá-los.
- Não suporta constraints dependentes: cada invariante é independente de todas as outras, o que é restritivo. Todas as invariantes acabam por ser avaliadas, independentemente de erros anteriores, que faça com que a avaliação das seguintes fique sem significado.
- Pouca flexibilidade na definição de contextos: uma invariante é sempre definida no contexto de uma meta-casse, o que é limitador caso seja necessário um particionamento mais fino.
- Não suporta reparação de inconsistências: como foi desenhado para não criar side effects, apenas detecta inconsistências, e não poderá modificar o modelo para as corrigir.
- Não suporta constraints inter-modelo: as constraints podem apenas ser analisadas no contexto de apenas um modelo, logo é impossível criar contraints que envolvam mais que um modelo.

### Síntese

As especificações EVL estão organizadas em módulos (EvlModule). Cada módulo extende a EolLibraryModule do EOL, que serve de núcleo a todas as linguagens do Epsilon. 
Cada módulo pode importar bibliotecas do EOL ou do EVL, pode envolver invariantes declaradas e avaliadas em contextos, e pode ter blocos pre e post para serem avaliados antes e depois das invariantes, respectivamente.

Os blocks da sintaxe do EVL são:

**context** cada context especifica diferentes tipos de instancias (Model, String, Interface, ...). Os constituintes de cada context são avaliados para validar o elemento instanciado do tipo context. 

**guard** cada context pode definir um guard opcional que restringe a avaliação das constraints no context.

**invariant** é uma classe abstracta que é o super-tipo da constraint e da critique. Pode também usar uma guard para evitar a avaliações desnecessárias. Cada invariande deve declarar uma condição de verificação (ou bloco de código), que devolve um valor booleano e é avaliado para cada um dos elementos do tipo context que satisfaçam o guard. 
Além disso, pode ser mostrar ao utilizador uma mensagem de feedback, e pode ser seguido de um bloco fix.

**fix** é um bloco que conhece o context, e serve para aplicar modificações para corrigir inconsistências.

**constraint** denotam erros críticos que invalidam o modelo. Constraint e critique ambos extendem invariant, e herdam todas as suas funcionalidades.

**critique** denotam um problema não crítico, mas que pede a atenção do utilizador.

**pre** e **post** definem blocos que serão executados antes e depois das invariants, respectivamente.

![Sintaxe abstracta de EVL](img/evl-sintaxe-abstracta.png)

#### Exemplo do uso da sintaxe
![Sintaxe abstracta de EVL](img/evl_ex_sintaxe.png)


### Semântica de execução

Um módulo EVL é executado com a seguinte semântica:

1. Execução das secções pre, na ordem que foram especificadas.

2. Colecciona as instâncias das meta-classes que foram definidas em cada contexto. De seguida avalia as guards, e para as invariantes não-lazy que satisfazerem a guard, é avaliado o body da invariant. 
Caso o body devolva false, a mensagem é avaliada, e tanto a invariante como os possíveis fixes são guardados na ValidationTrace.

3. O ValidationTrace é examinado, e as mensagens das constraints não satisfeitas é mostrada ao utilizador, e este pode seleccionar os fixes apropriados.

4. Executa os blocos post, se todos os fizes estiverem satisfeitos.


#### Dependências entre invariants

Existem as seguintes operações que permitem estabelecer dependências entre invariantes: *satisfies(invariant :String) : Boolean, satisfiesAll(invariants : Sequence(String)) : Boolean and satisfiesOne(invariants: Sequence(String)) : Boolean*.
Usando essas operações uma invariant pode especificar no seu guard outras invariants que têm que ser satisfeitas antes dela própria ser avaliada. 
Quando uma destas operações é chamada, o resultado da invariante é devolvido a partir de cache, se já tiver sido calculado; caso contrário, a invariante é avaliada no momento, e o seu valor devolvido.

### Exemplo: Padrão singleton

![Singleton](img/singleton.png)

O padrão singleton é amplamente utilizado, e é uma classe que permite que apenas uma instância seja criada. O método estático getInstance() devolve a única instância da classe.
Para assegurar que todos os singletons foram correctamente modelados, é necessário avaliar as seguintes invariants em todas as classes com o stereotype <<singleton>>:

- DefinesGetInstance: todas as classes devem ter o método getInstance().

- GetInstanceIsStatic: o método getInstance() deve ser estático.

- GetInstanceReturnsSame: O tipo de retorno do método getInstance() deve ser o tipo da própria classe.

Assim sendo as invariants GetInstanceIsStatic e GetInstanceReturnsSame dependem de DefinesGetInstance, pois, se o método não existir não faz sentido fazer as restantes verificações. 
Além disso, há accões que podem ser tomadas no bloco fix para corrigir os problemas. No caso do DefinesGetInstance o método getInstance() deve ser adicionado; no GetInstanceIsStatic o método pode ser mudado para estático; e, no GetInstanceReturnsSame, o tipo de retorno pode ser alterado.

#### Usando EVL para exprimir as invariantes


```evl
context Singleton {

	guard : self.stereotype−>exists(s|s.name = "singleton")
	
	constraint DefinesGetInstance {
		check : self.getGetInstanceOperation().isDefined() 
		message : "Singleton " + self.name + " must define a getInstance() operation" 
		fix {
		title : "Add a getInstance() operation to " + self.name 
			do {
				// Create the getInstance operation
				var op : new Operation;
				op.name = "getInstance"; op.owner = self;
				op.ownerScope = ScopeKind#sk_classifier;

				// Create the return parameter
				var returnParameter : new Parameter;
				returnParameter.type = self;
				op.parameter = Sequence{returnParameter};
				returnParameter.kind = ParameterDirectionKind#pdk_return;
			}
		}
	}
	
	constraint GetInstanceIsStatic {
		guard : self.satisfies ("DefinesGetInstance")
		check : self.getGetInstanceOperation().ownerScope = ScopeKind#sk_classifier
		message : " The getInstance() operation of singleton " + self.name + " must be static"
		fix {
			title : "Change to static " 
			do {
				self.getGetInstanceOperation.ownerScope = ScopeKind#sk_classifier;
			} 
		}
	}
	
	constraint GetInstanceReturnsSame {
		guard : self.satisfies ("DefinesGetInstance") 
		check {
			var returnParameter : Parameter; 
			returnParameter = self.getReturnParameter();
			return (returnParameter−>isDefined() and returnParameter.type = self);
		}
		message : " The getInstance() operation of singleton " + self.name + " must return " + self.name
		fix {
			title : " Change return type to " + self . name
			do {
				var returnParameter : Parameter;
				returnParameter = self.getReturnParameter();
				
				// If the operation does not have a return parameter create one
				if (not returnParameter.isDefined()){ 
					returnParameter = Parameter.newInstance();
					returnParameter.kind = ParameterDirectionKind#pdk_return; 
					returnParameter.behavioralFeature = self.getInstanceOperation(); 
				}
				
				// Set the correct return type
				returnParameter.type = self;
			}
		}
	} 
}

operation Class getGetInstanceOperation() : Operation {
	return self.feature.select(o:Operation|o.name = "getInstance").first();
}

operation Operation getReturnParameter() : Parameter { 
	return self.parameter.select (p:Parameter|p.kind = ParameterDirectionKind#pdk_return).first();
}
```

O context Singleton define que as invariantes em si contidas serão avaliadas nas instâncias do tipo Class de UML. O seu guard define que serão avaliadas apenas em classes que têm stereotype singleton. 
Assim, as invariantes não terão que verificar se estão a ser avaliadas contra singletons, como seria para OCL.

A constraint DefinesGetInstance não define nenhuma guard, pelo que será avaliada contra todos os singleton. 
No seu check, examina se a classe define o método chamada getInstance() ao invocar a operation getGetInstanceOperation(). Se isto falha, propõe uma correcção que adiciona o método em falta.

A constraint GetInstanceIsStatic define um guard que especifica que esta só pode ser avaliada se a constraint DefinesGetInstance tiver sido satisfeita. 
No seu check verifica que o método getInstance() é estático. Se a constraint não for satisfeita, então o seu bloco fiz pode ser chamado para mudar o scope de getInstance() para estático.

A constraint GetInstanceReturnsSame verifica que o tipo de retorno de getInstance() é o mesmo que o próprio singleton. 
Tal como a GetInstanceIsStatic, define quesó pode ser avaliada se a constraint DefinesGetInstance for satisfeita. 
SeGetInstanceReturnsSame falhar, invoca o seu bloco fix, altera o tipo de retorno é mudado para o correcto, além disso, se não existir um tipo de retorno, ele é adicionado.


## Conclusão


Enquanto que OCL se assemelha mais a um conceito de programação defensiva, ETL está muito mais próximo de programação por contracto, onde as condições são explicitamente verificadas nos guards. 
ETL permite mais facilmente especificar constraints mais concisas, minimizando a repetição de código, e, caso uma constraint não seja satisfeita para uma instância em particular, o utilizador recebe uma mensagem significativa, e é possível especificar formas automáticas de corrigir o modelo para reparar a inconsistência.

**Voltar para o readme.md principal**

* [https://bitbucket.org/mei-isep/research-topic-2019/src/master/team-013/](README.md)

## Bibliografia

Rouhi, Alireza & Zamani, Bahman. (2016). *Validating the application of design patterns using Epsilon*. 10.13140/RG.2.2.27748.07047. 

Dimitris Kolovos, Louis Rose, Antonio García-Domínguez, e Richard Paige. (29 de Julho de 2018). *The Epsilon Book*. Plublicado em: https://www.eclipse.org/epsilon/doc/book/