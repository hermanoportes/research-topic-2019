# Epsilon Generation Language (EGL) ##

O **EGL** é uma tecnologia responsável por transformar modelo em texto. 
**EGL** herda conceitos e lógica da **EOL** (Epsilon Object Language). 
É uma linguagem de gerar texto a partir de um template. No geral, 
é um analisador que gera uma árvore de sintaxe abstrata que 
compreende nós de saída estáticos e dinâmicos para um determinado modelo.
Contrariamente ao Acceleo, o mecanismo de transformação 
**EGL** possui um mecanismo de regras de transformação **EGX**. 
Além de coordenar a execução das regras, o **EGX** permite gerar vários ficheiros a partir de um único template.
O **EGL** herda as construções imperativas da **EOL**, 
fornece tipos de dados semelhantes aos de Java e suporta métodos definidos pelo utilizador em tipos de metamodelos.

## Transformação de modelo em texto (M2T) ##

É um processo importante na geração de modelo para texto. 
No entanto, com a transformação M2T a geração não se limita ao código: 

o processo de transformação pode produzir qualquer tipo de artefacto textual, 
incluindo documentação, especificações de requisitos, manuais etc., porque 
o texto gerado é independente da linguagem de destino. Contrariamente 
à transformação M2M, os targets das transformações M2T são textuais e, 
na maioria das vezes, com uma estrutura arbitrária que não estão em conformidade 
com nenhum metamodelo. 

Existem duas categorias de transformações M2T: Visitor based e Template-based: 

Em relação ao Template-based, lecionado nas aulas, são ficheiros de texto 
que representam a saída final de uma transformação de modelo para texto que contém espaços reservados. 
Os espaços reservados representam variáveis que devem ser preenchidas com dados existentes no modelo de origem. 
Os templates de texto geralmente são compostos por seções estáticas e dinâmicas. 
As seções estáticas contêm texto escrito literalmente como parte de uma saída da 
transformação e as seções dinâmicas contêm seções com espaços reservados. 
As seções dinâmicas são incluídas em tags (por exemplo, [% %],).

Em contraste com a abordagem visitor-based, a estrutura de um modelo 
é muito semelhante com a sintaxe da linguagem de destino (Java por exemplo). 
A maioria das linguagens M2T recentes suporta a geração de texto com base em modelos.´

## Feautures sobre EGL ##

•	Contrariamente ao Acceleo, as transformações 
**EGL** dispõe de regras de transformação dedicadas ao mecanismo de coordenação denominado **EGX**. 
Para além disso, para coordenar a execução das regras, **EGX**
permite gerar múltiplos ficheiros a partir de um único template. 

•	Em relação às ferramentas usadas nas aulas necessitaríamos de ATL para transformação 
de M2M e de Acceleo para transformações de M2T;

•	Permite modificar simultaneamente muitos modelos de metamodelos diferentes 

•	Não está limitado a modelos baseados em EMF;

•	Usar Templates(com parâmetros) de outros modelos;

•	Definir e chamar submodelos;

•	Permite misturar código gerado com o código escrito pelo programador;

# ALTERNATIVAS AO EGL #

**Template Transformation Toolkit (T4)**

Um modelo de texto T4, como outras linguagens é uma mistura de blocos de texto e controlo 
lógico que gera qualquer formulário de artefactos textuais a partir de modelos. 
O mecanismo de geração do T4 é amplamente dependente da estrutura e das linguagens 
.NET. Assim, a lógica de controlo é escrita como fragmentos do código do programa 
no Visual C # ou Visual Basic. O mecanismo de modelo T4 pode gerar qualquer 
tipo de artefacto textual. As transformações T4 são executadas em modelos baseados em UML. 
Uma falha importante do T4 é que não permite incorporar o código gerado com texto escrito 
pelo utilizador. O tempo de execução permite que o programador incorpore instruções do Visual 
Studio (por exemplo, <# = DateTime.Now #>) no template, que são efetuados quando a transformação é executada. 
 
![example](img/T4.png)

## Acceleo ##

ferramenta lecionada nas aulas de EDOM, é baseado em três componentes separados: 
o compilador, o mecanismo de geração e o tooling. Com Acceleo é possível ler 
qualquer modelo produzido por um metamodelo baseado em EMF. Uma transformação 
efectuada no Acceleo é composta por módulos, ficheiros de origem e ficheiros de destino, 
como verificado no incremento 4. Um módulo pode consistir em vários templates que descrevem 
os parâmetros necessários para gerar texto a partir de modelos. Um template main serve como ponto
de entrada da execução da transformação e é indicado pela presença de um @main na parte superior do template.
 
![example](img/acceleo.png)

## Xpand ##

Linguagem de template estático, cujos principais recursos são programação orientada a aspectos, 
transformação e validação de modelos, com algumas limitações nos tipos de operações que o XPAND 
pode executar. Este também regista informações entre elementos de origem e destino entre gerações de código.
 
![example](img/xpand.png)

## MOFScript ##

Desenvolvido para dar resposta à necessidade de padronizar as 
linguagens de transformação M2T. Funciona com modelo baseado em 
MOF, por exemplo, UML, RDBMS, WSDL, BPMN etc., 
e é fortemente influenciado pelo QVT. Uma transformação 
MOFScript é composta por um ou mais módulos, em que 
cada módulo contem regras de transformação. 
Uma regra do MOFScript é uma especialização dos mapeamentos operacionais
do QVT-Merge e as construções do MOFScript são especializações das construções do QVT-Merge.
As transformações podem ser importadas e reutilizadas de outras transformações.
 
![example](img/MOFScript.png)


## Modelos de emissor Java (JET)##

O Jet cria classes de implementação Java a partir de modelos EMF. 
As classes de implementação Java são usadas para gravar a saída de texto. 
A classe de implementação possui um método 'generate' usado para obter 
o resultado da string. Por outras palavras, a transformação é de objetos 
Java em texto. As tags de comando no JET são chamadas scriplets, denotadas por <%%>, 
estas podem conter qualquer código Java. 

Ao contrário do **EGL** e do Acceleo, o 
JET não suporta modelos implementados usando o MOF.
 
![example](img/JET.png)

https://www.eclipse.org/epsilon/examples/index.php?example=org.eclipse.epsilon.examples.egl.library

## Demonstração exemplo EGL ##

Este exemplo demonstra o uso do **EGL** para gerar código HTML a partir de um documento XML,  
representado na figura abaixo.
Como referido anteriormente, com o **EGL** é possível usar como ficheiro fonte documentos do 
tipo XML, EMF, UML e CSV. 
Para termos uma comparação da aplicação desta ferramenta com a que usamos durante as aulas Acceleo, 
o ideal seria usar um exemplo com ficheiro fonte do tipo EMF mas os exemplos encontrados eram muito 
mais extensos e complexos e assim optamos por explicar um exemplo com ficheiro de entrada XML.
Começamos por mostrar o ficheiro de entrada cuja root é uma library com uma lista de books.
 
![example](img/EGL_1.png)

Vamos gerar um ficheiro HTML inicialmente para passar uma library para um 
índex.html em que de seguida irá listar os elementos <book> que tenham 
o atributo público definido como true. Cada book terá uma opção clicável 
na pagina redirecionando para a lista de autores de cada livro. 

Abaixo podemos ver o template **EGL**, library2page.egl 
que foi utilizado para gerar um índex.html com intuito de listar os books. 
Como podemos constatar a sintaxe é bastante semelhante ao acceleo

 
![example](img/EGL_2.png)

De seguida foi criado um segundo template book2page.egl para 
listar os outros de cada livro selecionado anteriormente no índex anterior
book2page.egl

 
![example](img/EGL_3.png)

Como também já foi indicado anteriormente, o **EGL** também permite fazer transformações, 
semelhante ao ATL, usando um programa de coordenação **EGX** como o que está representado 
na figura abaixo (main.egx). A regra Book2Page do programa **EGX** 
transformará todos os elementos <book> (t_book) que satisfizerem a 
validação declarada (ter um atributo público definido como true), 
num ficheiro de destino, usando o modelo especificado (book2page.egl). 
Além disso, o programa EGX especifica uma regra Library2Page, 
que gera um índex.html para cada elemento <library> no documento.

 
![example](img/EGL_4.png)

Com os **templates EGL** definidos e as respetivas regras de transformação indicadas no **EGX** é 
possível correr um ficheiro .main localizado na figura abaixo é possível gerar o html final 
indicado na figura seguinte.
 
![example](img/EGL_5.png)


Apenas por curiosidade também é possível gerar o html final correndo uma aplicação Java semelhante a esta:

 
![example](img/EGL_6.png)


Como resultado final obtemos as seguintes funcionalidades:

 
![example](img/EGL_7.png)

 
![example](img/EGL_8.png)

 
![example](img/EGL_9.png)

**Voltar ao readme.MD principal**

* [https://bitbucket.org/mei-isep/research-topic-2019/src/master/team-013/](README.md)
